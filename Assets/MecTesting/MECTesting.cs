﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public class MECTesting : MonoBehaviour
{
	public TestingOptions TestingOptions;

	private const int NumberOfItems = 10000;
	private readonly bool[ ] _bools = new bool[ NumberOfItems ];
	private readonly WaitForEndOfFrame _waitForEndOfFrame = new WaitForEndOfFrame(  );

	private void Start ()
	{
		switch( TestingOptions )
		{
			case TestingOptions.Unity:
				for( int i = 0; i < NumberOfItems; i++ )
					StartCoroutine( UnityFlipItem( i ) );
				break;

			case TestingOptions.UnityOptimized:
				for( int i = 0; i < NumberOfItems; i++ )
					StartCoroutine( UnityOptimizedFlipItem( i ) );
				break;

			case TestingOptions.MEC:
				for( int i = 0; i < NumberOfItems; i++ )
					Timing.RunCoroutine( MECFlipItem( i ) );
				break;
		}
	}

	private IEnumerator UnityFlipItem( int index )
	{
		var number = index;
		while( number >= 0 )
		{
			_bools[ index ] = number % 2 == 0;
			number++;
			yield return new WaitForEndOfFrame( );
		}
	}

	private IEnumerator UnityOptimizedFlipItem( int index )
	{
		var number = index;
		while( number >= 0 )
		{
			_bools[ index ] = number % 2 == 0;
			number++;
			yield return _waitForEndOfFrame;
		}
	}

	private IEnumerator<float> MECFlipItem( int index )
	{
		var number = index;
		while( number >= 0 )
		{
			_bools[ index ] = number % 2 == 0;
			number++;
			yield return Timing.WaitForOneFrame;
		}
	}
}

public enum TestingOptions
{
	Unity,
	UnityOptimized,
	MEC,
}
