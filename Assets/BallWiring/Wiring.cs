﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wiring : MonoBehaviour
{
    public Vector3 Velocity;
    public Bounds Bounds;

    private LineRenderer line;
    private List< Vector3 > positions = new List< Vector3 >( );

//	private Dictionary< Collider2D, int > collisions = new Dictionary< Collider2D, int >( );
//	private static List< Wiring > others = new List< Wiring >( );

    private void Awake( )
    {
        line = GetComponent< LineRenderer >( );
//		others.Add( this );
    }

    // Update is called once per frame
    private void Update( )
    {
        transform.position += Velocity * Time.deltaTime;

        var posX = ( transform.position.x < Bounds.min.x ) ? Bounds.max.x :
            ( transform.position.x > Bounds.max.x ) ? Bounds.min.x : transform.position.x;
        var posY = ( transform.position.y < Bounds.min.y ) ? Bounds.max.y :
            ( transform.position.y > Bounds.max.y ) ? Bounds.min.y : transform.position.y;

        transform.position = new Vector3( posX, posY );


//		foreach( var other in others )
//		{
//			if( Vector2.Distance( other.transform.position, transform.position ) < 3.0f )
//			{
//				positions.Add( transform.position );
//				positions.Add( other.transform.position );
//			}
//		}
//
//		Debug.Log( positions.Count );
        line.positionCount = positions.Count;
        line.SetPositions( positions.ToArray( ) );
        positions.Clear( );
    }

//	private void OnTriggerEnter2D( Collider2D other )
//	{
////		collisions.Add( other, collisions.Count + 1 );
//		positions.Add( transform.position );
//		positions.Add( other.transform.position );
//	}

    private void OnTriggerStay2D( Collider2D other )
    {
        positions.Add( transform.position );
        positions.Add( other.transform.position );
    }

//	private void OnTriggerExit2D( Collider2D other )
//	{
////		collisions.Remove( other );
//		positions.RemoveAt(  );
//	}
}