﻿using System.Collections.Generic;
using MEC;
using UnityEngine;

public class SpawnBalls : MonoBehaviour
{
    public int BallCount = 100;
    public GameObject Ball;
    private Bounds bounds;

    // Use this for initialization
    private void Start( )
    {
        var camera = Camera.main;
        var screenAspect = Screen.width / ( float )Screen.height;
        var cameraHeight = camera.orthographicSize * 2;
        bounds = new Bounds( camera.transform.position, new Vector3( cameraHeight * screenAspect, cameraHeight, 0 ) );

        Timing.RunCoroutine( Spawn( ) );
    }

    private IEnumerator< float > Spawn( )
    {
        for( var i = 0; i < BallCount; i++ )
        {
            var posX = Random.Range( bounds.min.x, bounds.max.x );
            var posY = Random.Range( bounds.min.y, bounds.max.y );
            var ball = Instantiate( Ball, new Vector3( posX, posY ), Quaternion.identity, transform );
            var wiring = ball.GetComponent< Wiring >( );

            wiring.Velocity = new Vector3( Random.Range( -5.0f, 5.0f ), Random.Range( -5.0f, 5.0f ) );
            wiring.Bounds = bounds;

            yield return Timing.WaitForSeconds( 0.05f );
        }
    }
}