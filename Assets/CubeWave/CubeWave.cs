﻿using UnityEngine;

public class CubeWave : MonoBehaviour
{
    public int CubeX;
    public int CubeZ;
    public float SinSpeed = 3.0f;
    public float CubeSpacing = 1.25f;
    public float CubeHeight = 5.0f;
    public GameObject Cube;
    private Transform[] cubes;

    private void Awake()
    {
        cubes = new Transform[CubeX * CubeZ];
        var halfWd = Mathf.FloorToInt(CubeX / 2.0f + 0.5f);
        var halfHt = Mathf.FloorToInt(CubeZ / 2.0f + 0.5f);
        for(var x = 0; x < CubeX; x++)
        for(var z = 0; z < CubeZ; z++)
        {
            var pos = new Vector3(x + 1 - halfWd, 0, z + 1 - halfHt) * CubeSpacing;
            cubes[x * CubeX + z] = Instantiate(Cube, pos, Quaternion.identity).transform;
        }
    }

    private void Update( )
    {
        for(var i = 0; i < cubes.Length; i++)
        {
            var cube = cubes[i];
            var scale = cube.localScale;
            var distance = Vector3.Distance(Vector3.zero, cube.position) * 0.1f;
            var remapped = Mathf.Sin((-Time.time + distance) * SinSpeed) * CubeHeight + ( CubeHeight * 1.5f );
            scale.y = remapped;
            cube.localScale = scale;
        }
    }
}