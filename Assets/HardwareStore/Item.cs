﻿using UnityEngine;

//[CreateAssetMenu(fileName = "New Item", menuName = "Hardware Store/New Item")]
namespace HardwareStore
{
	public struct Item
	{
		public string Name;

		public int AmountSold;
		public int AmountOrdered;
		public int AmountInStore;

		public double ManufacturePrice;
		public double SellingPrice;

		public Item( string name,
		             double manufacturePrice,
		             double sellingPrice,
		             int amountSold,
		             int amountOrdered,
		             int amountInStore )
		{
			Name = name;
			AmountSold = amountSold;
			AmountOrdered = amountOrdered;
			AmountInStore = amountInStore;
			ManufacturePrice = manufacturePrice;
			SellingPrice = sellingPrice;
		}
	}
}
