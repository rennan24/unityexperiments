﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System.IO;
using System.Linq;
using UnityEngine.UI;

namespace HardwareStore
{
	public class InitializeList : MonoBehaviour
	{
		public List< Item > Items;

		public Scrollbar Scrollbar;
		public RectTransform ParentTransform;
		public GameObject ItemEntry;

		private IEnumerator Start( )
		{
			var lines = File.ReadAllLines( @"Assets\HardwareStore\HardwareStoreItems.txt" );
			var numItems = lines.Length / 3;

			yield return new WaitForSeconds( 0.5f );

			for ( var i = 0; i < numItems; i++ )
			{
				var entry = Instantiate( ItemEntry, ParentTransform ).GetComponent< RectTransform >( );
				//var words = lines[ i * 3 + 2 ].Split( ' ' ).Where( t => !string.IsNullOrWhiteSpace( t ) ).ToArray( );

				StartCoroutine( MoveTo( entry, new Vector2( 400, -i * entry.sizeDelta.y ), 1.0f ) );

				var texts = entry.GetComponentsInChildren< Text >( );
				texts[ 0 ].text = lines[ i * 3 + 0 ];
				texts[ 1 ].text = lines[ i * 3 + 1 ];
				//texts[ 2 ].text = words[ 0 ];
				//texts[ 3 ].text = words[ 1 ];
				//texts[ 4 ].text = words[ 2 ];

				ParentTransform.sizeDelta = new Vector2( 0, ( i + 1 ) * entry.sizeDelta.y );

				yield return new WaitForSeconds( 0.05f );
			}
		}

		public IEnumerator MoveTo( RectTransform rectTransform, Vector2 startPosition, float seconds )
		{
			var startingPosition = startPosition;
			var endingPosition = new Vector2( 0, startPosition.y );

			for ( var t = 0.0f; t <= 1.0f; t += Time.deltaTime / seconds )
			{
				rectTransform.anchoredPosition = Vector2.Lerp( startingPosition, endingPosition, EaseInOut( t ) );
				yield return new WaitForEndOfFrame( );
			}

			rectTransform.anchoredPosition = endingPosition;
		}

		public float EaseInOut( float t )
		{
			return t * t * ( 3.0f - 2.0f * t );
		}
	}
}
