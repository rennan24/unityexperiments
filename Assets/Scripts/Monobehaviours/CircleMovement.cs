﻿using UnityEngine;

public class CircleMovement : MonoBehaviour
{
	public float Speed;
	public float Radius;
	public Vector3 Center;
	public Vector3 Offset;

	private int flipper;

	private void Start( )
	{
		flipper = Random.value < 0.5f ? 1 : -1;
	}

	// Update is called once per frame
	private void Update( )
	{
		var movement = new Vector3( Mathf.Sin( Time.time + Offset.x ) * Radius * flipper, 0, Mathf.Cos( Time.time + Offset.x ) * Radius );
		transform.rotation = Quaternion.LookRotation( -transform.position );
		transform.position = Center + movement;
	}
}
