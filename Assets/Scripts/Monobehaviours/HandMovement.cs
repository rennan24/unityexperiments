﻿using System;
using UnityEngine;

public class HandMovement : MonoBehaviour
{
    public float Amplitude;
    public float Frequency;
    public float Variance;

    private Vector3 startPosition;

    private void Start()
    {
        startPosition = transform.position;
    }

    private void Update()
    {
        var position = startPosition;
        position.x += Mathf.Cos(Time.time * Frequency - Variance + Mathf.Sin(Time.time + Variance)) * Amplitude * transform.localScale.x;
        position.y += Mathf.Sin(Time.time * Frequency + Variance) * Amplitude;
        transform.position = position;
    }
}
