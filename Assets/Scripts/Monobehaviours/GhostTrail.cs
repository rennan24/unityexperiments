﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostTrail : MonoBehaviour
{
	public float FadeSpeed = 1.0f;
	public float RepeatRate = 0.2f;

	private float _repeatTime;

	private void Update( )
	{
		if( _repeatTime < Time.timeSinceLevelLoad )
		{
			var trail = new GameObject( $"{name} Trail" );
			
			var trailRenderer = trail.AddComponent< SpriteRenderer >( );
			trailRenderer.sprite = GetComponent< SpriteRenderer >( ).sprite;
			trailRenderer.material = GetComponent< SpriteRenderer >( ).sharedMaterial;

			trail.transform.SetPositionAndRotation( transform.position, transform.rotation );
			trail.transform.localScale = transform.localScale;
			
			StartCoroutine( nameof( FadeTrailPart ), trailRenderer );
			_repeatTime = Time.timeSinceLevelLoad + RepeatRate;
		}
	}

	private IEnumerator FadeTrailPart( SpriteRenderer trailRenderer )
	{
		var startColor = trailRenderer.color;
		for( var t = 1.0f; t > 0.0f; t -= Time.deltaTime / FadeSpeed )
		{
			startColor.a = t;
			trailRenderer.color = startColor;
			trailRenderer.sortingOrder -= 1;
			yield return null;
		}

		Destroy( trailRenderer.gameObject );
	}
}
