﻿using UnityEngine;
using System.Collections.Generic;
public class Pooling : MonoBehaviour
{
	public GameObject Object;
	public int PoolSize = 50;

	private int poolIndex;
	private List< GameObject > objectPool;

	public void Start ( )
	{
		objectPool = new List<GameObject> ( PoolSize );
		for ( int i = 0; i < PoolSize; i++ )
		{
			var go = Instantiate( Object, Vector3.zero, Quaternion.identity );
			go.SetActive ( false );

			objectPool [ i ] = go;
		}
	}

	public GameObject GetObject ( )
	{
		if ( objectPool.Count == 0 ) return null;

		var go = objectPool[ poolIndex++ ];
		go.SetActive ( true );
		return go;
	}
}
