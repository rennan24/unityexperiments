﻿using UnityEngine;

[ RequireComponent( typeof( MeshFilter ) ) ]
[ RequireComponent( typeof( MeshRenderer ) ) ]
public class ColorWheelMesh : MonoBehaviour
{
	public SpriteRenderer Square;
	public float Radius = 2;

	[ Range( 0, 1 ) ] public float Brightness;

	private MeshFilter filter;
	private Vector2 direction = Vector2.up;

	private void Awake( )
	{
		filter = GetComponent< MeshFilter >( );
	}

	private void Start( )
	{
		filter.mesh = CreateCircleMesh( );
	}

	private void OnDrawGizmosSelected( )
	{
		for( var i = 0; i < 360; i++ )
		{
			Gizmos.color = Color.HSVToRGB( ( float ) i / 360, 1, 1 );
			Gizmos.DrawSphere( Quaternion.Euler( 0, 0, i ) * Vector3.up * Radius, 0.1f );
		}
	}

	private void Update( )
	{
		SetBrightness( );
		
		if( Input.GetMouseButton( 0 ) )
		{
			var mousePosition = new Vector3( Input.mousePosition.x, Input.mousePosition.y, 10 );
			var worldPosition = Camera.main.ScreenToWorldPoint( mousePosition );
			direction = worldPosition - transform.position;

			var angle = Quaternion.FromToRotation( Vector3.up, direction ).eulerAngles.z;
			Square.color = SampleWheel( ( int ) angle, direction.magnitude );
		}
	}

	private Color SampleWheel( int degree, float distance )
	{
		return Color.HSVToRGB( ( float ) degree / 360, distance / Radius, Brightness );
	}

	private void SetBrightness( )
	{
		var colors = filter.mesh.colors;

		colors[ 360 ] = new Color( Brightness, Brightness, Brightness, 1 );

		for( var i = 0; i < 360; i++ )
			colors[ i ] = Color.HSVToRGB( ( float ) i / 360, 1, Brightness );

		filter.mesh.colors = colors;
	}

	private Mesh CreateCircleMesh( )
	{
		var mesh = new Mesh { name = "CircleMesh" };

		var colors = new Color[ 361 ];
		var vertices = new Vector3[ 361 ];
		var triangles = new int [ 360 * 3 ];

		colors[ 360 ] = new Color( Brightness, Brightness, Brightness, 1 );
		vertices[ 360 ] = transform.position;

		for( var i = 0; i < 360; i++ )
		{
			colors[ i ] = Color.HSVToRGB( ( float ) i / 360, 1, Brightness );
			vertices[ i ] = Quaternion.Euler( 0, 0, i ) * Vector3.up * Radius;

			triangles[ i * 3 ] = 360;
			triangles[ i * 3 + 1 ] = ( i + 1 ) % 360;
			triangles[ i * 3 + 2 ] = i;
		}

		mesh.vertices = vertices;
		mesh.colors = colors;
		mesh.triangles = triangles;

		return mesh;
	}
}
