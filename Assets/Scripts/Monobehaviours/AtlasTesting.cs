﻿using UnityEngine;
using UnityEngine.U2D;

public class AtlasTesting : MonoBehaviour
{
	public SpriteAtlas Atlas;


	private Sprite[ ] sprites;
	private Texture2D atlas;

	private void Awake( )
	{
		sprites = new Sprite[ Atlas.spriteCount ];
		Atlas.GetSprites( sprites );
		atlas = sprites[ 0 ].texture;

		var spriteRenderer = gameObject.AddComponent< SpriteRenderer >( );
		spriteRenderer.sprite = Sprite.Create( atlas, new Rect( 0, 0, atlas.width, atlas.height ), Vector2.zero );
	}

	private void OnDrawGizmos( )
	{
		if( sprites == null ) return;
		foreach( var sprite in sprites )
		{
			var center = new Vector3( sprite.textureRect.center.x / sprite.pixelsPerUnit, sprite.textureRect.center.y / sprite.pixelsPerUnit );
			var size = new Vector3( sprite.textureRect.size.x / sprite.pixelsPerUnit, sprite.textureRect.size.y / sprite.pixelsPerUnit );
			Gizmos.DrawWireCube( transform.position + Vector3.Scale( transform.localScale, center ), Vector3.Scale( transform.localScale, size ) );
		}
	}
}
