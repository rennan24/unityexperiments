﻿using UnityEngine;

public class TilingOffset : MonoBehaviour
{
	public Vector2 Direction;

	private Material material;
	private Vector2 startingOffset;

	private void Awake( )
	{
		material = GetComponent< Renderer >( ).sharedMaterial;
		startingOffset = material.mainTextureOffset;
	}

	private void OnDestroy( )
	{
		material.mainTextureOffset = startingOffset;
	}

	void Update( )
	{
		material.mainTextureOffset += Direction * Time.deltaTime;
	}
}