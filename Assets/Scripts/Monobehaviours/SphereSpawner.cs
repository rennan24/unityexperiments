﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SphereSpawner : MonoBehaviour
{
	public GameObject Quad;
	public float Amount;
	private void Start( )
	{
		
		var parent = new GameObject( "Quad Sphere" );
		for ( var i = 0; i < Amount; i++ )
		{
			var quad = Instantiate( Quad, Random.onUnitSphere * 5, Quaternion.identity, parent.transform ).GetComponent<CircleMovement>(  );
			quad.GetComponent<MeshRenderer>(  ).material.color = Random.ColorHSV( );
			quad.Center = new Vector3( parent.transform.position.x, quad.transform.position.y, parent.transform.position.z );
			quad.Radius = Vector3.Distance( quad.transform.position, new Vector3 ( parent.transform.position.x, quad.transform.position.y, parent.transform.position.z )  );
			quad.Offset = quad.transform.position;
		}
	}
}
