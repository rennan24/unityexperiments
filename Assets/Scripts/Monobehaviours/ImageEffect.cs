﻿using UnityEngine;

public class ImageEffect : MonoBehaviour
{
	public Material ImageEffectMaterial;

	private void OnRenderImage( RenderTexture src, RenderTexture dest )
	{
		Graphics.Blit( src, dest, ImageEffectMaterial );
	}
}
