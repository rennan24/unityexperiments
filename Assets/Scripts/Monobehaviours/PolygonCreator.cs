﻿using System.Collections.Generic;
using UnityEngine;

public class PolygonCreator : MonoBehaviour
{
	private MeshFilter meshFilter;
	private Mesh mesh;

	private PolygonCollider2D polygon;

	private List<Vector3> points = new List< Vector3 >();
	private List<int> triangles = new List< int >();

	private void OnEnable ( )
	{
		MouseEvents.MouseClickHandler += OnMouseClick;
	}

	private void OnDisable ( )
	{
		MouseEvents.MouseClickHandler -= OnMouseClick;
	}

	private void Start ( )
	{
		meshFilter   = GetComponent<MeshFilter> ( );
		mesh         = new Mesh { name = "Polygon Creator Mesh" };

		meshFilter.mesh = mesh;
	}

	private void OnMouseClick ( MouseButton mouseButton, Vector2 mouseScreenPosition, Vector2 mouseWorldPosition )
	{
		if ( mouseButton != MouseButton.Left ) return;

		points.Add ( mouseWorldPosition );
		triangles.Add ( triangles.Count );

		mesh.vertices = points.ToArray ( );
		mesh.triangles = triangles.ToArray ( );
	}

}
