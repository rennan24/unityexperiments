﻿using UnityEngine;

public class ConstantVelocity : MonoBehaviour
{
	public Vector3 Velocity;

	private void Update ()
	{
		transform.position += Velocity * Time.deltaTime;
	}
}
