﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeirdMovement : MonoBehaviour
{
	private GameObject [ ] objects;
	private LineRenderer _lineRenderer;
	public int NumberOfPoints = 30;
	public float Radius = 2;
	public float Speed = 2;


	// Use this for initialization
	private void Start ( )
	{
		_lineRenderer = GetComponent < LineRenderer > ( );
		_lineRenderer.positionCount = NumberOfPoints;
		objects = new GameObject[ NumberOfPoints ];
		for ( int i = 0; i < NumberOfPoints; i++ )
		{
			objects [ i ] = GameObject.CreatePrimitive ( PrimitiveType.Sphere );
			objects [ i ].transform.localScale = new Vector3 ( 0.1f, 0.1f, 0.1f );
		}
	}

	// Update is called once per frame
	private void Update ( )
	{
		for ( int i = 0; i < NumberOfPoints; i++ )
		{
			var r = ( float ) i / NumberOfPoints * Speed;
			var position = new Vector3 ( Mathf.Cos ( Time.time * r ) * Radius, Mathf.Sin ( Time.time * r ) * Radius, objects [ i ].transform.position.z );
			_lineRenderer.SetPosition ( i, position );
			objects [ i ].transform.position = position;
		}
	}
}
