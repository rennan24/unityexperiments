﻿using UnityEngine;

public class RaycastTesting : MonoBehaviour
{
	public float RayLength = 5;
	public ContactFilter2D ContactFilter;
	
	private RaycastHit2D[] hits = new RaycastHit2D[1];

	private void FixedUpdate( )
	{
		GetRayDistance( transform.position, transform.rotation * Vector2.down, RayLength );
	}

	private float GetRayDistance( Vector2 position, Vector2 direction, float distance )
	{
		var hitCount = Physics2D.Raycast( position, direction, ContactFilter, hits, distance );

		if( hitCount != 0 )
		{
			var hitInfo = hits[ 0 ];
			Debug.DrawRay( position, direction * hitInfo.distance, Color.red );

			var newPosition = hitInfo.point;
			var newDistance = distance - hitInfo.distance;
			var newDirection = direction - 2 * Vector2.Dot( direction, hitInfo.normal ) * hitInfo.normal;
			var hitDistance = GetRayDistance( newPosition, newDirection, newDistance );

			Debug.DrawRay( newPosition, hitInfo.normal, Color.yellow );
			Debug.DrawRay( newPosition, newDirection * hitDistance, Color.red );

			return hitInfo.distance;
		}

		Debug.DrawRay( position, direction * distance, Color.green );
		return 0;
	}
}
