﻿using UnityEngine;

public class CircularMovement : MonoBehaviour
{
	public float Radius   = 2.0f;
	public float Speed    = 5.0f;
	public Vector3 Offset = Vector3.zero;

	private Vector3 center;

	private void Start ( )
	{
		center = transform.position + Offset;
	}

	private void Update ( )
	{
		transform.position = center + new Vector3 ( Mathf.Sin ( Time.time * Speed ) * Radius, Mathf.Cos( Time.time * Speed ) * Radius, 0 );
	}
}
