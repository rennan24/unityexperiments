﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PingPongMovement : MonoBehaviour
{
	public Vector3 Length;
	public float Speed;

	private Vector3 center;
	private Vector3 length;

	void Start ( )
	{
		center = transform.position;
	}

	void Update ( )
	{
		length = new Vector3 ( Mathf.PingPong ( Time.time * Speed, Length.x ), Mathf.PingPong ( Time.time * Speed, Length.y ) );
		transform.position = center + length;
	}
}
