﻿using UnityEngine;

public class MouseEvents : MonoBehaviour
{
	public delegate void MouseClickEventHandler ( MouseButton mouseButton, Vector2 mouseScreenPosition, Vector2 mouseWorldPosition );

	public static event MouseClickEventHandler MouseClickHandler;

	private void Start ( )
	{
		// Debug Stuff
//		MouseClickHandler += ( button, position, worldPosition ) => {
//			Debug.Log ( string.Format ( "Mouse Button: {0}, Screen Position{1}, World Position: {2}", button, position, worldPosition ) );
//		};
	}

	private void Update ( )
	{
		if ( MouseClickHandler == null ) return;

		var mouseScreenPosition = Input.mousePosition;
		var mouseWorldPosition  = Camera.main.ScreenToWorldPoint( mouseScreenPosition );

		if ( Input.GetMouseButtonDown ( 0 ) )
			MouseClickHandler.Invoke ( MouseButton.Left, mouseScreenPosition, mouseWorldPosition );
		else if ( Input.GetMouseButtonDown ( 1 ) )
			MouseClickHandler.Invoke ( MouseButton.Right, mouseScreenPosition, mouseWorldPosition );
		else if ( Input.GetMouseButtonDown ( 2 ) )
			MouseClickHandler.Invoke ( MouseButton.Middle, mouseScreenPosition, mouseWorldPosition );
	}
}

public enum MouseButton
{
	None,
	Left,
	Right,
	Middle,
}
