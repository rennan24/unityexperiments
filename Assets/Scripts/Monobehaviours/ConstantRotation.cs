﻿using UnityEngine;

public class ConstantRotation : MonoBehaviour
{
	public Vector3 RotationSpeed;

	public void Update ( )
	{
		transform.Rotate ( RotationSpeed * Time.deltaTime );
	}
}
