﻿using System.Text.RegularExpressions;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class WebPageTest : MonoBehaviour
{
	public RectTransform UI;
	public GameObject TextField;
	public GameObject ImageField;
	public int Columns;
	public int Rows;

	public Sprite[ ] sprites;
	public string[ ] names;

	private float xSpacing;
	private float ySpacing;

	private void Start( )
	{
		names = new string[ Columns * Rows ];
		sprites = new Sprite[ Columns * Rows ];
		xSpacing = UI.rect.width / Columns;
		ySpacing = UI.rect.height / Rows;

		for ( int i = 0; i < Columns * Rows; i++ )
			StartCoroutine( FillWithRandomNames( "http://www.behindthename.com/random/random.php?number=1&gender=both&surname=&norare=yes&all=no&usage_eng=1", i ) );

		for ( int i = 0; i < Columns * Rows; i++ )
			StartCoroutine( GetURLTexture( "https://unsplash.it/250/250?random", i ) );

	}

	private IEnumerator GetURLTexture( string URL, int index )
	{
		var imageWd = UI.rect.width / Columns;
		var imageHt = UI.rect.height / Rows;

		WWW webpage = new WWW("Test.com");
		do
		{
			//webpage = new WWW($"https://unsplash.it/{imageWd}/{imageHt}?random");
			//yield return webpage;
		} while ( webpage.texture == null );

        yield return null;
		var x = index % Columns;
		var y = index / Columns % Rows;

		webpage.texture.wrapMode = TextureWrapMode.Repeat;
		sprites[ index ] = Sprite.Create( webpage.texture, new Rect( 0, 0, webpage.texture.width, webpage.texture.height ), new Vector2( 0.5f, 0.5f ) );

		var image = Instantiate( ImageField, new Vector3( ( x * imageWd ) + ( imageWd / 2 ), ( y * imageHt ) + ( imageHt / 2 ) ), Quaternion.identity, UI.transform ).GetComponent<Image>(  );
		image.rectTransform.sizeDelta = new Vector2( imageWd, imageHt );
		image.sprite = sprites[ index ];
	}

	private IEnumerator FillWithRandomNames( string URL, int index )
	{
		var webpage = new WWW( URL );
		yield return webpage;
		names[ index ] = Regex.Match( webpage.text, @"href=\""/name/.*?\"">(.*?)</a>" ).Groups[ 1 ].Value;

		var x = index % Columns;
		var y = index / Columns % Rows;

		var obj = Instantiate( TextField, new Vector3( ( x * xSpacing ) + ( xSpacing / 2 ), ( y * ySpacing ) + ( ySpacing / 2 ) ), Quaternion.identity, UI.transform );
		obj.GetComponent< Text >( ).text = names[ index ];
	}
}
