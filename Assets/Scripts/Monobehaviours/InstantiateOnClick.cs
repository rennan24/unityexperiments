﻿using UnityEngine;

public class InstantiateOnClick : MonoBehaviour
{
	public GameObject InstantiatedObject;

	private Transform parentTransform;

	private void Start ( )
	{
		parentTransform = new GameObject ( "Instantiation Parent" ).transform;
	}

	private void Update ( )
	{
		if ( Input.GetMouseButton ( 0 ) )
		{
			var rotation = Quaternion.Euler( 0, 0, Random.Range( 0, 360 ) );
			var worldMousePos = Camera.main.ScreenToWorldPoint( new Vector3( Input.mousePosition.x, Input.mousePosition.y, 10 ) );
			Instantiate ( InstantiatedObject, worldMousePos, rotation, parentTransform );
			Instantiate ( InstantiatedObject, worldMousePos, rotation, parentTransform );
			Instantiate ( InstantiatedObject, worldMousePos, rotation, parentTransform );
		}

	}
}
