﻿using System.Collections;
using UnityEngine;

public class TraceObject : MonoBehaviour
{
	public Transform TracedObject = null;
	public float TraceTime        = 1.0f;
	public float DelayTime        = 1.0f;
	public Gradient TracedColor   = null;

	private Vector3 oldPosition;
	private Vector3 newPosition;

	private IEnumerator Start ( )
	{
		yield return new WaitForSeconds ( DelayTime );
		newPosition = oldPosition = TracedObject.position;

		while ( true )
		{
			newPosition = TracedObject.position + new Vector3 ( 0, 0, -1 );
			Debug.DrawLine ( oldPosition, newPosition, TracedColor.Evaluate ( Mathf.PingPong ( Time.time, 1 ) ), TraceTime, true );
			oldPosition = newPosition;

			yield return new WaitForEndOfFrame ( );
		}
	}
}
