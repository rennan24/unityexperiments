﻿Shader "Unlit/Checkerboard"
{
	Properties {
	    [PerRendererData] _MainTex ("Texture", 2D) = "white" {}
		_CheckerSize ( "Checker Size", float ) = 4
		_Color ( "Tint", Color ) = ( 1, 1, 1, 1 )
		_ColorOdd ( "Color Odd", Color ) = ( 1, 1, 1, 1 )
		_ColorEven ( "Color Even", Color ) = ( 0, 0, 0, 1 )
	}

	SubShader {
		Tags { "Queue"="Transparent" "RenderType" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off
		Cull Off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#pragma multi_compile _ PIXELSNAP_ON​
			#include "UnityCG.cginc"

			struct appdata
			{
			    UNITY_VERTEX_INPUT_INSTANCE_ID
				float4 pos : POSITION;
				float4 color : COLOR;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
				float4 pos : SV_POSITION;
				float3 objPos : OBJPOS;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _MainTex_TexelSize;
			float _CheckerSize;
			fixed4 _Color;
			fixed4 _ColorOdd;
			fixed4 _ColorEven;


			v2f vert (appdata v)
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				o.objPos = v.pos;
				o.pos = UnityObjectToClipPos( v.pos );
				o.uv = TRANSFORM_TEX ( v.uv, _MainTex );
				o.color = v.color * _Color;

				return o;
			}

			fixed4 frag ( v2f i ) : SV_Target
			{
			    fixed4 color = tex2D( _MainTex, i.uv ) * i.color;
				float total = floor ( _CheckerSize * i.objPos.x + _Time.y ) + floor ( _CheckerSize * i.objPos.y - _Time.y );
				return color * ( ( total % 2.0 == 0.0 ) ? _ColorEven : _ColorOdd );
			}
			ENDCG
		}
	}
}
