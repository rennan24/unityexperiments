﻿Shader "Unlit/WobbleShader"
{
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_WobbleSpeedX ( "Wobble Speed X", Float ) = 0
		_WobbleSpeedY ( "Wobble Speed Y", Float ) = 0
	}

	SubShader {
		Tags { "Queue"="Transparent" "RenderType" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off
		Cull Off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _MainTex_TexelSize;
			fixed _WobbleSpeedX;
			fixed _WobbleSpeedY;
			
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.color = v.color;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed wobbleX = ( sin ( ( _Time.x + i.uv.y ) * _WobbleSpeedX ) * 5 ) / _MainTex_TexelSize.z;
				fixed wobbleY = ( sin ( ( _Time.x + i.uv.x ) * _WobbleSpeedY ) * 5 ) / _MainTex_TexelSize.w;

				float2 texPos = fixed2 ( wobbleX, wobbleY ) + i.uv;
				return tex2D( _MainTex, texPos ) * i.color;
			}
			ENDCG
		}
	}
}
