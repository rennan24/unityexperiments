﻿Shader "Unlit/ChromaticAbberation"
{
	Properties {
	    [PerRendererData] _MainTex ("Texture", 2D) = "white" {}
		_CheckerSize ( "Checker Size", float ) = 4
		_Color ( "Tint", Color ) = ( 1, 1, 1, 1 )
		_Speed ( "Speed", Range ( 1, 10 ) ) = 1
		_Magnitude ( "Magnitude", Range ( 0, 1 ) ) = 0.1
	}

	SubShader {
		Tags { "Queue"="Transparent" "RenderType" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off
		Cull Off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _ PIXELSNAP_ON​
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 pos : POSITION;
				float4 color : COLOR;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
				float4 pos : SV_POSITION;
				float3 objPos : OBJPOS;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _MainTex_TexelSize;
			fixed4 _Color;
            float _Magnitude;
            float _Speed;

			v2f vert (appdata v)
			{
				v2f o;
				o.objPos = v.pos;
				o.pos = UnityObjectToClipPos( v.pos );
				o.uv = TRANSFORM_TEX ( v.uv, _MainTex );
				o.color = v.color * _Color;

				#ifdef PIXELSNAP_ON
				o.pos = UnityPixelSnap ( o.pos );
				#endif​

				return o;
			}

             float rand(float3 co)
             {
                 return frac ( sin ( dot ( co.xyz, float3 ( 12.9898, 78.233, 45.5432 ) ) ) );
             }

			fixed4 frag ( v2f i ) : SV_Target
			{
			    float moveX = ( sin ( _Time.y * _Speed * rand ( i.objPos ) ) * _Magnitude );
			    float moveY = ( cos ( _Time.y * _Speed * rand ( i.objPos ) ) * _Magnitude );

			    fixed4 color = tex2D( _MainTex, fixed2 ( i.uv.x, i.uv.y ) );
			    color.r = tex2D( _MainTex, fixed2 ( i.uv.x + moveX, i.uv.y ) ).r;
			    color.g = tex2D( _MainTex, fixed2 ( i.uv.x, i.uv.y ) ).g;
			    color.b = tex2D( _MainTex, fixed2 ( i.uv.x, i.uv.y + moveY ) ).b;
				return color * i.color;
			}
			ENDCG
		}
	}
}
