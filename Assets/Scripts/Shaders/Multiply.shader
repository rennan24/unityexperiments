﻿Shader "Unlit/Multiply"
{
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_MultiplyTexture ( "Texture", 2D) = "white" {}
		_AlphaThreshold ("Alpha Threshold", Range( 0.0, 1.0 ) ) = 0.5
		_NoiseSpeedX ( "Noise Speed X", float ) = 0.0
		_NoiseSpeedY ( "Noise Speed Y", float ) = 0.0
	}

	SubShader {
		Tags { "Queue"="Transparent" "RenderType" = "Transparent" "DisableBatching" = "true" }
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off
		Cull Off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
				float4 color : COLOR;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			sampler2D _MultiplyTexture;
			float4 _MultiplyTexture_ST;
			float4 _MultiplyTexture_TexelSize;
			float4 _MainTex_ST;
			float4 _MainTex_TexelSize;
			half _AlphaThreshold;
			half _NoiseSpeedX;
			half _NoiseSpeedY;


			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.uv2 = TRANSFORM_TEX(v.uv, _MultiplyTexture);
				o.color = v.color;
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
			    float2 noiseOffset = float2( _NoiseSpeedX, _NoiseSpeedY ) * _Time.x;
			    float4 noise = tex2D( _MultiplyTexture, i.uv2 + noiseOffset );
			    float4 color = tex2D( _MainTex, i.uv ) * i.color;
			    noise.a = saturate( ( noise.r - _AlphaThreshold ) / 0.25f );
                noise.rgb = 1;
			    return color * noise;
			}
			ENDCG
		}
	}
}
