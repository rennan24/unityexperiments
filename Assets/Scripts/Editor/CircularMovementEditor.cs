﻿using UnityEditor;
using UnityEngine;

[CustomEditor ( typeof ( CircularMovement ) )]
public class CircularMovementEditor : Editor
{
	private CircularMovement t;

	private void OnEnable ( )
	{
		t = target as CircularMovement;
	}

	private void OnSceneGUI ( )
	{
		Handles.DrawWireDisc ( t.transform.position + t.Offset, Vector3.forward, t.Radius );
	}
}
