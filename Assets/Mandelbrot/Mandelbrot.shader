﻿Shader "Experiments/Mandelbrot"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_ColorTex ("Noise", 2D) = "white" {}
		_Center ("Center", Vector ) = ( 1, 1, 0, 0 )
		_Scale ( "Scale", Float ) = 0
		_Iterations ( "Iterations", Int ) = 0
	}
	SubShader
	{
		// No culling or depth
		Cull Off 
		ZWrite On 

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _ColorTex;

            float2 _Center;
            float _Scale;
            int _Iterations; 

			fixed4 frag (v2f IN) : SV_Target
			{
                float2 z, c;
            
                c.x = 1.3333 * (IN.uv.x - 0.5) * _Scale - _Center.x;
                c.y = (IN.uv.y - 0.5) * _Scale - _Center.y;
            
                float i;
                z = c;
                
                for(i=0; i<_Iterations; i++) {
                    float x = (z.x * z.x - z.y * z.y) + c.x;
                    float y = (z.y * z.x + z.x * z.y) + c.y;
            
                    if((x * x + y * y) > 4.0) break;
                    z.x = x;
                    z.y = y;
                }
            
				return tex2D( _ColorTex, (i == _Iterations ? 0.0 : float(i)) / 100.0 );
			}
			ENDCG
		}
	}
}
