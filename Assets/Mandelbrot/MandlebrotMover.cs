﻿using UnityEngine;

public class MandlebrotMover : MonoBehaviour
{
	public Material MandelbrotMaterial;

	public float MovementSpeed;

	public Vector4 Center;
	public float Scale = 1;

	private void Update( )
	{
		var horizontalInput = Input.GetAxis( "Horizontal" );
		var verticalInput = Input.GetAxis( "Vertical" );
		var scroll = Input.GetAxis( "Mouse ScrollWheel" );

		Center += Time.deltaTime * Scale * 0.75f * new Vector4( -horizontalInput * MovementSpeed, -verticalInput * MovementSpeed, 0, 0 );
		Scale -= scroll * MovementSpeed * 100 * Time.deltaTime * Scale;


		MandelbrotMaterial.SetVector( "_Center", Center );
		MandelbrotMaterial.SetFloat( "_Scale", Scale );
	}
}
