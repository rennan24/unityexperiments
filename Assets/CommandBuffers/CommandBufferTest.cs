﻿using UnityEngine;
using UnityEngine.Rendering;

public class CommandBufferTest : MonoBehaviour
{
    public int instanceCount = 100000;
    public Mesh instanceMesh;
    public Material instanceMaterial;
    public int subMeshIndex = 0;

    private CommandBuffer commandBuffer;

    private const int MinInstanceCount = 1;
    private const int MaxInstanceCount = 100000;

    private void Start()
    {
        commandBuffer = new CommandBuffer();

        var propBlock = new MaterialPropertyBlock();

        for (int i = 0; i < 100; i++)
        {
            var mat = Matrix4x4.TRS(Vector3.right * i, Quaternion.identity, Vector3.one);

            propBlock.SetColor("_Color", Random.ColorHSV(0.0f, 1.0f));
            commandBuffer.DrawMesh(instanceMesh, mat, instanceMaterial);
        }

    }



//	private int cachedInstanceCount = -1;
//	private int cachedSubMeshIndex = -1;
//	private ComputeBuffer positionBuffer;
//	private ComputeBuffer argsBuffer;
//	private uint[] args = new uint[5] {0, 0, 0, 0, 0};
//
//	private void Start()
//	{
//		argsBuffer = new ComputeBuffer(1, args.Length * sizeof(uint), ComputeBufferType.IndirectArguments);
//		UpdateBuffers();
//	}

    private void Update()
    {
//		Update starting position buffer
//		if (cachedInstanceCount != instanceCount || cachedSubMeshIndex != subMeshIndex)
//			UpdateBuffers();

        // Pad input
        if (Input.GetAxisRaw("Horizontal") != 0.0f)
            instanceCount = Mathf.Clamp(instanceCount + (int)(Input.GetAxis("Horizontal") * 1000), MinInstanceCount, MaxInstanceCount);

        // Render
        Graphics.ExecuteCommandBuffer(commandBuffer);
//		Graphics.DrawMeshInstancedIndirect(instanceMesh, subMeshIndex, instanceMaterial, new Bounds(Vector3.zero, new Vector3(100.0f, 100.0f, 100.0f)), argsBuffer);
    }

    private void OnGUI()
    {
        GUI.Label(new Rect(265, 25, 200, 30), "Instance Count: " + instanceCount);
        instanceCount = (int) GUI.HorizontalSlider(new Rect(25, 20, 200, 30), instanceCount, MinInstanceCount, MaxInstanceCount);
    }

//	private void UpdateBuffers()
//	{
//		// Ensure submesh index is in range
//		if (instanceMesh != null)
//			subMeshIndex = Mathf.Clamp(subMeshIndex, 0, instanceMesh.subMeshCount - 1);
//
//		// Positions
//		positionBuffer?.Release();
//		positionBuffer = new ComputeBuffer(instanceCount, 16);
//		Vector4[] positions = new Vector4[instanceCount];
//		for (int i = 0; i < instanceCount; i++)
//		{
//			float angle = Random.Range(0.0f, Mathf.PI * 2.0f);
//			float distance = Random.Range(20.0f, 100.0f);
//			float height = Random.Range(-2.0f, 2.0f);
//			float size = Random.Range(0.05f, 0.25f);
//			positions[i] = new Vector4(Mathf.Sin(angle) * distance, height, Mathf.Cos(angle) * distance, size);
//		}
//
//		positionBuffer.SetData(positions);
//		instanceMaterial.SetBuffer("positionBuffer", positionBuffer);
//
//		// Indirect args
//		if (instanceMesh != null)
//		{
//			args[0] = (uint) instanceMesh.GetIndexCount(subMeshIndex);
//			args[1] = (uint) instanceCount;
//			args[2] = (uint) instanceMesh.GetIndexStart(subMeshIndex);
//			args[3] = (uint) instanceMesh.GetBaseVertex(subMeshIndex);
//		}
//		else
//		{
//			args[0] = args[1] = args[2] = args[3] = 0;
//		}
//
//		argsBuffer.SetData(args);
//
//		cachedInstanceCount = instanceCount;
//		cachedSubMeshIndex = subMeshIndex;
//	}

//	private void OnDisable()
//	{
//		positionBuffer?.Release();
//		positionBuffer = null;
//
//		argsBuffer?.Release();
//		argsBuffer = null;
//	}
}