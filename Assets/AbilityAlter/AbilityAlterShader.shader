﻿Shader "Unlit/AbilityAlterShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_AdditiveTex ("Additive Texture", 2D) = "white" {}
		_CutoffTex ("Cutoff Texture", 2D) = "white" {}
		_Cutoff ("Cutoff Range", Range(0.0, 3.0)) = 1
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			sampler2D _AdditiveTex;
			sampler2D _CutoffTex;
			float4 _MainTex_ST;
			float _Cutoff;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 additive = tex2D(_AdditiveTex, i.uv);
				fixed4 cutoff = tex2D(_CutoffTex, i.uv);

				_Cutoff = fmod(_Time.y, 2);
				if(cutoff.r > 1 - _Cutoff && cutoff.r < 1 - (_Cutoff -0.5))
				{
                    col.rgb += smoothstep(additive.a, 0, cutoff.r);
				}
				return col;
			}
			ENDCG
		}
	}
}
