﻿using System;
using UnityEngine;

public class FiniteState<TContext, TStateEnum>
{
    public virtual bool TestOut() => true;
    public virtual bool TestIn() => true;
    public virtual void Update() { }
    public virtual void Enter() { }
    public virtual void Exit() { }
    public TStateEnum Transitions;
    public TStateEnum Name;
    public TContext Agent;
    public float ElapsedTime;

    public FiniteState(TContext agent, TStateEnum transitions, TStateEnum name)
    {
        Agent = agent;
        Transitions = transitions;
        Name = name;
    }
}

public class FiniteStateMachine<TContext, TStateEnum> where TStateEnum : struct, IConvertible
{
    public TStateEnum Current
    {
        get { return States[CurrentStateIndex].Name; }
        set
        {
            for (var i = 0; i < States.Length; i++)
            {
                if (States[i].Name.Equals(value))
                    CurrentStateIndex = i;
            }
        }
    }

    private int currentStateIndex;
    public int CurrentStateIndex
    {
        get { return currentStateIndex; }
        set
        {
            States[currentStateIndex].ElapsedTime = 0.0f;
            States[currentStateIndex].Exit();
            currentStateIndex = Mathf.Clamp(value, 0, States.Length);
            States[currentStateIndex].Enter();
            States[currentStateIndex].ElapsedTime = 0.0f;
        }
    }

    public bool Enabled = true;
    public FiniteState<TContext, TStateEnum>[] States;

    public FiniteStateMachine(TStateEnum startingState, FiniteState<TContext, TStateEnum>[] states)
    {
        States = states;
        Current = startingState;
    }

    public void Update()
    {
        if (!Enabled) return;

        States[currentStateIndex].ElapsedTime += Time.deltaTime;
        States[currentStateIndex].Update();
        CheckStateChange();
    }

    private void CheckStateChange()
    {
        var currentState = States[CurrentStateIndex];
        var transitions = currentState.Transitions;

        // Iterate through all the states
        for (var transitionIndex = 0; transitionIndex < States.Length; transitionIndex++)
        {
            var transitionState = States[transitionIndex];

            // Check to see if we can even transition into this state
            if ((Convert.ToInt32(transitions) & Convert.ToInt32(transitionState.Name)) == 0) continue;
            if (!transitionState.TestIn()) continue;
            if (!currentState.TestOut()) continue;

            CurrentStateIndex = transitionIndex;
            return;
        }
    }
}