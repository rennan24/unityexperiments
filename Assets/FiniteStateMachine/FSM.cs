﻿using UnityEngine;

namespace BZillions
{
    public abstract class FSMState<TContext>
    {
        public virtual bool TestOut() { return true; }
        public virtual bool TestIn() { return true; }
        public virtual void Update() { }
        public virtual void Enter() { }
        public virtual void Exit() { }
        public int Transitions;
        public int StateFlag;
        public float ElapsedTime;
        public bool CanRepeat;

        protected TContext Agent;

        public FSMState(TContext context, int transitions = 0)
        {
            Agent = context;
            Transitions = transitions;
        }
    }

    public class FSM<TContext>
    {
        public int CurrentState
        {
            get { return States[CurrentStateIndex].StateFlag; }
            set
            {
                for (var i = 0; i < States.Length; i++)
                {
                    if (States[i].StateFlag == value)
                        CurrentStateIndex = i;
                }
            }
        }

        private int currentStateIndex;
        public int CurrentStateIndex
        {
            get { return currentStateIndex; }
            set
            {
                States[currentStateIndex].Exit();
                States[currentStateIndex].ElapsedTime = 0.0f;
                currentStateIndex = Mathf.Clamp(value, 0, States.Length);
                States[currentStateIndex].ElapsedTime = 0.0f;
                States[currentStateIndex].Enter();
            }
        }

        public void TryState(int value)
        {
            for (var i = 0; i < States.Length; i++)
            {
                if (States[i].StateFlag == value)
                {
                    // Return if we can't repeat states
                    if (currentStateIndex == i && !States[currentStateIndex].CanRepeat)
                        return;

                    // Test to see if we can enter the state
                    if (States[i].TestIn())
                        CurrentStateIndex = i;
                }
            }
        }

        public bool Enabled = true;
        public FSMState<TContext>[] States;

        private float timer = -1;

        public FSM(FSMState<TContext>[] states, int startState)
        {
            States = states;

            for (var i = 0; i < States.Length; i++)
            {
                if (States[i].StateFlag == startState)
                    currentStateIndex = i;
            }

            States[currentStateIndex].Enter();
        }

        public void Pause(float seconds)
        {
            timer = Time.time + seconds;
        }

        public void Update()
        {
            if (!Enabled)
                return;

            States[currentStateIndex].ElapsedTime += Time.deltaTime;
            States[currentStateIndex].Update();

            if(Time.time > timer)
                CheckStateChange();
        }

        private void CheckStateChange()
        {
            var currentState = States[CurrentStateIndex];
            var transitions = currentState.Transitions;

            // Iterate through all the states
            for (var transitionIndex = 0; transitionIndex < States.Length; transitionIndex++)
            {
                var transitionState = States[transitionIndex];

                // Skip Checking yourself
                if(currentStateIndex == transitionIndex && !currentState.CanRepeat) continue;

                // Check to see if we can even transition into this state
                if ((transitions & transitionState.StateFlag) == 0) continue;
                if (!transitionState.TestIn()) continue;
                if (!currentState.TestOut()) continue;

                CurrentStateIndex = transitionIndex;
                return;
            }
        }
    }
}