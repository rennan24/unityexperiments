﻿using System;
using UnityEngine;

public partial class SimpleEnemy : MonoBehaviour
{
    public float WalkSpeed = 2.5f;
    public float JumpStrength = 10.0f;
    public SimpleEnemyState StartingSimpleEnemyState;
    public Color IdleColor = Color.white;
    public Color WalkColor = Color.yellow;
    public Color JumpColor = Color.green;
    public Color FallColor = Color.red;

    private bool grounded;
    private bool moving;
    private bool jumped;

    private const float RayLength = 0.6f;
    private Rigidbody2D body2D;
    private SpriteRenderer spriteRenderer;
    private FiniteStateMachine< SimpleEnemy, SimpleEnemyState > stateMachine;

    private void Awake( )
    {
        body2D = GetComponent< Rigidbody2D >( );
        spriteRenderer = GetComponent< SpriteRenderer >( );

        stateMachine = new FiniteStateMachine< SimpleEnemy, SimpleEnemyState >(
            StartingSimpleEnemyState,
            new FiniteState< SimpleEnemy, SimpleEnemyState >[ ] {
                new JumpState( this, SimpleEnemyState.Fall ),
                new FallState( this, SimpleEnemyState.Walk | SimpleEnemyState.Idle ),
                new WalkState( this, SimpleEnemyState.Jump | SimpleEnemyState.Idle ),
                new IdleState( this, SimpleEnemyState.Jump | SimpleEnemyState.Walk ),
            }
        );
    }

    private void Update( )
    {
        var direction = Input.GetAxis( "Horizontal" );
        body2D.velocity = new Vector2( WalkSpeed * direction, body2D.velocity.y );

//		if( Input.GetKeyDown( KeyCode.Space ) )
//			stateMachine.CurrentState = SimpleEnemyStates.Jump;
        jumped = Input.GetKeyDown( KeyCode.Space );
        moving = Mathf.Abs( body2D.velocity.x ) > 0;
        grounded = Physics2D.Raycast( transform.position, Vector2.down, RayLength );
        Debug.DrawRay( transform.position, Vector3.down * RayLength, Color.cyan );
        stateMachine.Update( );
    }
}

// States
public partial class SimpleEnemy
{
    private class IdleState : FiniteState< SimpleEnemy, SimpleEnemyState >
    {
        public IdleState(SimpleEnemy agent, SimpleEnemyState transitions) : base(agent, transitions, SimpleEnemyState.Idle) { }

        public override bool TestIn( )
            => Agent.grounded && Mathf.Abs( Agent.body2D.velocity.x ) < 0.1f;

        public override void Enter( )
        {
            Agent.spriteRenderer.color = Agent.IdleColor;
        }
    }

    private class WalkState : FiniteState< SimpleEnemy, SimpleEnemyState >
    {
        public WalkState(SimpleEnemy agent, SimpleEnemyState transitions) : base(agent, transitions, SimpleEnemyState.Walk) { }

        public override bool TestIn( )
            => Agent.grounded && Mathf.Abs( Agent.body2D.velocity.x ) > 0.1f;

        public override void Enter( )
            => Agent.spriteRenderer.color = Agent.WalkColor;

    }

    private class JumpState : FiniteState< SimpleEnemy, SimpleEnemyState >
    {
        public JumpState(SimpleEnemy agent, SimpleEnemyState transitions) : base(agent, transitions, SimpleEnemyState.Jump) { }

        public override bool TestIn( )
            => Agent.jumped;

        public override void Enter( )
        {
            Agent.spriteRenderer.color = Agent.JumpColor;
            Agent.body2D.velocity = new Vector2( Agent.body2D.velocity.x, Agent.JumpStrength );
        }
    }

    private class FallState : FiniteState< SimpleEnemy, SimpleEnemyState >
    {
        public FallState(SimpleEnemy agent, SimpleEnemyState transitions) : base(agent, transitions, SimpleEnemyState.Fall) { }

        public override bool TestIn( )
            => Agent.body2D.velocity.y < -0.5f;

        public override void Enter( )
        {
            Agent.spriteRenderer.color = Agent.FallColor;
        }
    }
}

[ Flags ]
public enum SimpleEnemyState
{
    Jump = 1 << 0,
    Idle = 1 << 1,
    Walk = 1 << 2,
    Fall = 1 << 3,
}