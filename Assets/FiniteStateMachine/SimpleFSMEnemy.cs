﻿using System;
using UnityEngine;

namespace SimpleFSM
{
    public class SimpleFSMEnemy : SimpleFSM
    {
        public float WalkSpeed = 2.5f;
        public float JumpStrength = 10.0f;
        public Color IdleColor = Color.white;
        public Color WalkColor = Color.yellow;
        public Color JumpColor = Color.green;
        public Color FallColor = Color.red;

        private bool grounded;
        private bool moving;
        private bool jumped;

        private const float RayLength = 0.6f;
        private Rigidbody2D body2D;
        private SpriteRenderer spriteRenderer;

        private void Awake()
        {
            body2D = GetComponent<Rigidbody2D>();
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        protected override void Update()
        {
            var direction = Input.GetAxis("Horizontal");
            body2D.velocity = new Vector2(WalkSpeed * direction, body2D.velocity.y);

            //		if( Input.GetKeyDown( KeyCode.Space ) )
            //			stateMachine.CurrentState = SimpleEnemyStates.Jump;
            jumped = Input.GetKeyDown(KeyCode.Space);
            moving = Mathf.Abs(body2D.velocity.x) > 0;
            grounded = Physics2D.Raycast(transform.position, Vector2.down, RayLength);
            Debug.DrawRay(transform.position, Vector3.down * RayLength, Color.cyan);
            base.Update();
        }

        private class IdleState : State<SimpleFSMEnemy>
        {
            public IdleState(SimpleFSMEnemy agent) : base(agent) { }

            public override void Enter()
            {
                Agent.spriteRenderer.color = Agent.IdleColor;
            }
        }

        private class WalkState : State<SimpleFSMEnemy>
        {
            public WalkState(SimpleFSMEnemy agent) : base(agent) { }

            public override void Enter()
                => Agent.spriteRenderer.color = Agent.WalkColor;
        }

        private class JumpState : State<SimpleFSMEnemy>
        {
            public JumpState(SimpleFSMEnemy agent) : base(agent) { }

            public override void Enter()
            {
                Agent.spriteRenderer.color = Agent.JumpColor;
                Agent.body2D.velocity = new Vector2(Agent.body2D.velocity.x, Agent.JumpStrength);
            }
        }

        private class FallState : State<SimpleFSMEnemy>
        {
            public FallState(SimpleFSMEnemy agent) : base(agent) { }

            public override void Enter()
            {
                Agent.spriteRenderer.color = Agent.FallColor;
            }
        }
    }
}