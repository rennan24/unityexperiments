﻿using UnityEngine;

namespace SimpleFSM
{

    public class SimpleFSM : MonoBehaviour
    {
        public IState CurrentState
        {
            get { return currentState; }
            set
            {
                if (currentState != null)
                {
                    currentState.Exit();
                    currentState.TimeElapsed = 0.0f;
                }

                currentState = value;
                currentState.Enter();
            }
        }

        private IState currentState;

        private float timer = -1;

        public void Pause(float seconds)
        {
            timer = Time.time + seconds;
        }

        protected virtual void Update()
        {
            currentState.Update();
            currentState.TimeElapsed += Time.deltaTime;
        }
    }

    public interface IState
    {
        void Update();
        void Enter();
        void Exit();
        float TimeElapsed { get; set; }
    }

    public abstract class State<TAgent> : IState
    {
        public virtual void Update() { }
        public virtual void Enter() { }
        public virtual void Exit() { }
        public float TimeElapsed { get; set; }
        protected TAgent Agent;

        protected State(TAgent agent)
        {
            Agent = agent;
        }
    }
}