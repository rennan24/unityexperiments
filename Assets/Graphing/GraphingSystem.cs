﻿using UnityEngine;

public class GraphingSystem : MonoBehaviour
{
	public float GraphSize = 1.0f;

	public const int MaxResolution = 100;
	public const int MinResolution = 10;

	[ Range( MinResolution, MaxResolution ) ]
	public int GraphResolution = 10;

	public Functions CurrentFunction;

	public delegate float GraphFunctionDelegate( float x );

	public GraphFunctionDelegate GraphFunction = Linear;

	private ParticleSystem particles;
	private ParticleSystem.Particle[ ] points;

	private void Start( )
	{
		particles = GetComponent< ParticleSystem >( );
		points = new ParticleSystem.Particle[ MaxResolution ];
	}

	private void OnValidate( )
	{
		switch( CurrentFunction )
		{
			case Functions.Linear:
				GraphFunction = Linear;
				break;
			case Functions.Parabolic:
				GraphFunction = Parabolic;
				break;
			case Functions.Exponential:
				GraphFunction = Exponential;
				break;
			case Functions.Sine:
				GraphFunction = Sine;
				break;
		}
	}

	private void Update( )
	{
		var increment = GraphSize / ( GraphResolution - 1 );
		for( var i = 0; i < GraphResolution; i++ )
		{
			var x = i * increment;
			points[ i ].position = new Vector3( x, 0f, 0f );
			points[ i ].startColor = new Color( x, 0f, 0f );
			points[ i ].startSize = particles.main.startSize.constant;
		}

		for( var i = 0; i < GraphResolution; i++ )
		{
			var p = points[ i ].position;
			p.y = GraphFunction( p.x );
			points[ i ].position = p;
		}
		particles.SetParticles( points, GraphResolution );
	}

	private static float Linear( float x )
	{
		return x;
	}

	private static float Exponential( float x )
	{
		return x * x;
	}

	private static float Parabolic( float x )
	{
		x = 2f * x - 1f;
		return x * x;
	}

	private static float Sine( float x )
	{
		return 0.5f * Mathf.Sin( 2 * Mathf.PI * ( x + Time.time ) );
	}

	public enum Functions
	{
		Linear,
		Parabolic,
		Exponential,
		Sine,
	}
}
